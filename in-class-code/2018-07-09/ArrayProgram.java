public class ArrayProgram
{
    public static void main()
    {
        String[] classes = new String[4];
        classes[0] = "CS134";
        classes[1] = "CS210";
        classes[2] = "ENG100";
        classes[3] = "PHIL200";
        
        classes[2] = "";
        
        System.out.println( classes.length + " total classes..." );
        for ( int i = 0; i < classes.length; i++ )
        {
            System.out.println( "*\t" + i + ": " + classes[i] );
        }        
    }
}
