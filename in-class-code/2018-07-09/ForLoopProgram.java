public class ForLoopProgram
{
    public static void main()
    {
        System.out.println( "Count up by 2's:" );
        for ( int i = 0; i <= 10; i += 2 )
        {
            System.out.print( i + " " );
        }
        
        System.out.println("");
        System.out.println("");
        System.out.println( "Count down by 2's:" );
        for ( int i = 10; i >= 0; i -= 2 )
        {
            System.out.print( i + " " );
        }
        
        System.out.println("");
        System.out.println("");
        System.out.println( "Count up, multiply by 2 each time:" );
        for ( int i = 1; i <= 100; i *= 2 )
        {
            System.out.print( i + " " );
        }
        
        System.out.println("");
        System.out.println("");
        System.out.println( "Count up, divide by 2 each time:" );
        for ( double i = 100; i > 1; i /= 2.0 )
        {
            System.out.print( i + " " );
        }
    }
}
