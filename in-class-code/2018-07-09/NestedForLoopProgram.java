public class NestedForLoopProgram
{
    public static void main()
    {
        for ( int hour = 0; hour < 24; hour++ )
        {
            for ( int min = 0; min < 60; min++ )
            {
                System.out.println( hour + ":" + min );
            }
        }
    }
}
