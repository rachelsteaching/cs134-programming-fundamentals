import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class Enemy extends Actor
{
    public void act() 
    {
        Move();
    }   
    
    public void Move()
    {
        // Get the World
        MyWorld world = (MyWorld)getWorld();
        
        // Get the Player to track it
        Player player = world.GetPlayer();
        
        turnTowards( player.getX(), player.getY() );
        move( 1 );
    }
}
