import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class MyWorld extends World
{
    Player player1 = new Player();
    Player player2 = new Player();
    Enemy enemy = new Enemy();
    
    public MyWorld()
    {    
        super(600, 400, 1); 
        
        player1.SetSpeed( 6 );
        player2.SetSpeed( 3 );
        
        player1.turn( -5 );
        
        addObject( player1, 200, 100 );
        addObject( player2, 200, 300 );
        addObject( enemy, 400, 200 );
    }
    
    public Player GetPlayer()
    {
        return player1;
    }
    
    public Enemy GetEnemy()
    {
        return enemy;
    }
}
