import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class Player extends Actor
{
    int hp = 100;
    int speed = 5;
    int counter = 0;4
    int onImage = 1;
    
    public void act() 
    {
        Move();
        ShowHealth();
        CheckCollision();
        
        counter++;
        
        if ( counter == 10 )
        {
            setImage( "asdf" );
            counter = 0;
        }
    }   
    
    public void ShowHealth()
    {
        getWorld().showText( "HP: " + hp, 60, 20 );
    }
    
    public void SetSpeed( int spd )
    {
        speed = spd;
    }
    
    public void Move()
    {
        int x = getX();
        int y = getY();
        
        if ( Greenfoot.isKeyDown( "a" ) )
        {
            x -= speed;
        }
        else if ( Greenfoot.isKeyDown( "d" ) )
        {
            x += speed;
        }
        
        if ( Greenfoot.isKeyDown( "w" ) )
        {
            y -= speed;
        }
        else if ( Greenfoot.isKeyDown( "s" ) )
        {
            y += speed;
        }
        
        setLocation( x, y );
    } 
    
    public void CheckCollision()
    {
        Actor collided = (Actor)getOneIntersectingObject( Actor.class );
        
        if ( collided != null )
        {
            DealWithCollision( collided );
        }
    }
    
    public void DealWithCollision( Actor collided )
    {
        hp -= 1;
        if ( hp <= 0 )
        {
            getWorld().showText( "GAME OVER", 300, 200 );
            Greenfoot.stop();
        }
    }
}
