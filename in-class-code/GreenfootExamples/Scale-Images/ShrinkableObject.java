import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class ShrinkableObject extends Actor
{    
    public void act() 
    {
        if ( Greenfoot.isKeyDown( "down" ) )
        {
            Shrink();
        }
        else if ( Greenfoot.isKeyDown( "up" ) )
        {
            Grow();
        }
    }
    
    void Shrink()
    {
        // Get the current image and modify it.
        GreenfootImage image = getImage();
        int width = image.getWidth();
        int height = image.getHeight();
        
        width /= 2;
        height /= 2;
        
        if ( width > 0 && height > 0 )
        {
            image.scale( width, height );
        }
    }
    
    void Grow()
    {
        // Get the current image and modify it.
        GreenfootImage image = getImage();
        int width = image.getWidth();
        int height = image.getHeight();
        
        width *= 2;
        height *= 2;
        
        image.scale( width, height );
    }
}
