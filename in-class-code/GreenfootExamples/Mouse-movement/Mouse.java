import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class Mouse extends Actor
{
    public void act() 
    {
        Move();
        CheckCollision();
    }  
    
    public void Move()
    {
        MouseInfo mouse = Greenfoot.getMouseInfo();
        
        if ( mouse != null )
        {
            int mouseX = mouse.getX();
            int mouseY = mouse.getY();
            
            // Move the player to the mouse's location
            setLocation( mouseX, mouseY );
        }
    }
    
    public void CheckCollision()
    {
        Apple collided = (Apple)getOneIntersectingObject(Apple.class);
        if ( collided != null )
        {
            Greenfoot.playSound("mouse.wav");
            getWorld().removeObject( collided );
        }
    }
}
