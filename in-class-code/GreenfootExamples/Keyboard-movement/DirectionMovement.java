import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class DirectionMovement extends Actor
{
    int speed = 5;
    
    public void act() 
    {
        Move();
    }   
    
    public void Move()
    {
        // Get Actor's current x, y coordinates.
        int x = getX();
        int y = getY();
        
        if ( Greenfoot.isKeyDown( "up" ) )
        {
            y -= speed;
        }
        else if ( Greenfoot.isKeyDown( "down" ) )
        {
            y += speed;
        }
        
        if ( Greenfoot.isKeyDown( "left" ) )
        {
            x -= speed;
        }
        else if ( Greenfoot.isKeyDown( "right" ) )
        {
            x += speed;
        }
        
        // Update the location
        setLocation( x, y );
    }
}
