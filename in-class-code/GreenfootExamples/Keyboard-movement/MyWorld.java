import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class MyWorld extends World
{
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
        
        showText( "Submarine: WASD to move", 200, 20 );
        showText( "Crab: Arrow keys to move", 200, 40 );
        
        addObject( new RotationMovement(), 300, 200 );
        
        addObject( new DirectionMovement(), 300, 300 );
    }
}
