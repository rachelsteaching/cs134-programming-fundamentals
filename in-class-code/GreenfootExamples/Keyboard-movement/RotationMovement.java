import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class RotationMovement extends Actor
{
    int speed = 5;
    
    public void act() 
    {
        Move();
    }   
    
    public void Move()
    {
        if ( Greenfoot.isKeyDown( "w" ) )
        {
            move( speed );
        }
        else if ( Greenfoot.isKeyDown( "s" ) )
        {
            move( -speed );
        }
        
        if ( Greenfoot.isKeyDown( "a" ) )
        {
            turn( -speed );
        }
        else if ( Greenfoot.isKeyDown( "d" ) )
        {
            turn( speed );
        }
    }  
}
