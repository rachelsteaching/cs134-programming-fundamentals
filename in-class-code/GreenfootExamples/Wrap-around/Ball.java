import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class WrappableObject extends Actor
{
    public Ball()
    {
        setRotation( 35 );
    }
    
    public void act() 
    {
        Move();
        WrapAround();
        
        getWorld().showText( "X " + getX() + ", Y " + getY(), 100, 20 );
    }   
    
    public void Move()
    {
        if ( Greenfoot.isKeyDown( "up" ) )
        {
            move( 5 );
        }
        
        if ( Greenfoot.isKeyDown( "left" ) )
        {
            turn( -5 );
        }
        else if ( Greenfoot.isKeyDown( "right" ) )
        {
            turn( 5 );
        }
    }
    
    public void WrapAround()
    {        
        int worldWidth = getWorld().getWidth();
        int worldHeight = getWorld().getHeight();
        
        int playerX = getX();
        int playerY = getY();
        
        if ( playerX > worldWidth - 5 )
        {
            // Hit the right edge of the screen
            playerX = 5;
        }
        else if ( playerX < 5 )
        {   
            // Hit the left edge of the screen
            playerX = worldWidth - 5;
        }
        
        if ( playerY > worldHeight - 5 )
        {
            // Hit the bottom edge of the screen
            playerY = 5;
        }
        else if ( playerY < 5 )
        {
            // Hit the top edge of the screen
            playerY = worldHeight - 5;
        }
        
        setLocation( playerX, playerY );
    }
}
