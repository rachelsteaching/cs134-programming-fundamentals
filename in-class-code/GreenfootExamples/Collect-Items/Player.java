import greenfoot.*; 

public class Player extends Actor
{
    int itemsLeft = 5;  // member variable
    
    public void act() 
    {
        CheckCollision();
        Move();
    }   
    
public void CheckCollision()
{
    Collectable collided = (Collectable)getOneIntersectingObject( Collectable.class );
    
    if ( collided != null )
    {
        DealWithCollision( collided );
    }
}
    
    public void DealWithCollision( Actor collided )
    {
        itemsLeft--;
        Greenfoot.playSound( "collect.wav" );
        getWorld().removeObject( collided );
        CheckIfWin();
    }
    
    public void CheckIfWin()
    {
        if ( itemsLeft == 0 )
        {
            getWorld().showText( "You win!", 300, 200 );
            Greenfoot.stop();
        }
    }
    
    public void Move()
    {
        if ( Greenfoot.isKeyDown( "left" ) )
        {
            turn( -5 );
        }
        else if ( Greenfoot.isKeyDown( "right" ) )
        {
            turn( 5 );
        }
        
        if ( Greenfoot.isKeyDown( "up" ) )
        {
            move( 5 );
        }
        else if ( Greenfoot.isKeyDown( "down" ) )
        {
            move( -5 );
        }
    }
}
