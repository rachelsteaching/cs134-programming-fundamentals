import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class MyWorld extends World
{
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
        
        GreenfootSound backgroundMusic = new GreenfootSound("bunnies.mp3");
        //backgroundMusic.playLoop();
        
        showText( "Lobster Game", 300, 10 );
        
        addObject( new Player(), 300, 200 );
        
        // Create 5 starfish in random places
        for ( int i = 0; i < 5; i++ )
        {
            int x = Greenfoot.getRandomNumber( 600 );
            int y = Greenfoot.getRandomNumber( 400 );
            addObject( new Collectable(), x, y );
        }
    }
}
