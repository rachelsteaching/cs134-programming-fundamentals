import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class MyWorld extends World
{

    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
        
        for ( int i = 0; i < 10; i++ )
        {
            addObject( new FallingObject(), i * 60, 10 );
        }
    }
}
