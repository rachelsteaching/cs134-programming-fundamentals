import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class FallingObject extends Actor
{
    int speed = Greenfoot.getRandomNumber( 3 ) + 1;
    
    public void act() 
    {
        Move();
        WrapAround();
    } 
    
    public void Move()
    {
        setLocation( getX(), getY() + speed );
    }
    
    public void WrapAround()
    {
        // If it hits the ground, put it back at the top
        if ( getY() > getWorld().getHeight() - 5 )
        {
            setLocation( getX(), 0 );
        }
    }
}
