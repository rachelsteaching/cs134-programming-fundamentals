import java.util.Scanner;

public class TicketPriceProgram
{
    public static void main()   // Program starts here
    {
        double ticketPrice = 0;
        String isSenior;
        Scanner input = new Scanner( System.in );
        
        System.out.print( "Are you a senior citizen? (y/n): ");
        isSenior = input.nextLine();
        
        if ( isSenior.equals("y") )     // Is a senior
        {
            ticketPrice = 5.00;
        }
        else                            // Not a senior
        {
            ticketPrice = 10.00;
        }
        
        System.out.print( "Ticket price is: " );
        System.out.print( ticketPrice );
    }
}
