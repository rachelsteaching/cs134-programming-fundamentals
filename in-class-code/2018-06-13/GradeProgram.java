import java.util.Scanner;

public class GradeProgram
{
    public static void main()
    {
        Scanner input = new Scanner(System.in);
        
        System.out.print( "Enter your grade (0 - 100): " );
        int grade;
        grade = input.nextInt();
        
        if ( grade >= 90 && grade <= 100 )
        {
            System.out.println("A");
        }
        else if ( grade >= 80 && grade <= 89 )
        {
            System.out.println("B");
        }
        else if ( grade >= 70 && grade <= 79 )
        {
            System.out.println("C");
        }
        else if ( grade >= 60 && grade <= 69 )
        {
            System.out.println("D");
        }
        else
        {
            System.out.println("F");
        }
        
    }
}