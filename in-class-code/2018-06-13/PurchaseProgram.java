import java.util.Scanner;

public class PurchaseProgram
{
    public static void main()
    {
        Scanner input = new Scanner(System.in);
        
        int count;
        double totalPrice = 0.00;
        System.out.print( "How many purchases? " );
        count = input.nextInt();
        
        while ( count > 0 )
        {
            double price;
            System.out.print( "What is the price? $" );
            price = input.nextDouble();
            
            totalPrice = totalPrice + price;
            
            count = count - 1;
        }
        
        System.out.println( "Total cost: " + totalPrice );
    }
}
