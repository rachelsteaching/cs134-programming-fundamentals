import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class Perso extends Actor
{
    int moveAmount = 5;
    
    public void act() 
    {
        turn(moveAmount);
        move(5);
        
        if ( getRotation() >= 355 )
        {
            moveAmount = -moveAmount;
        }
        
        getWorld().showText( String.valueOf(getRotation()), 20, 20 );
        getWorld().showText( String.valueOf(moveAmount), 20, 40 );
    }    
}
